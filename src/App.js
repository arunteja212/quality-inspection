import { useLoader } from '@react-three/fiber'
import { VRCanvas, DefaultXRControllers, Interactive} from '@react-three/xr'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader'
import {OrbitControls, MeshReflectorMaterial} from "@react-three/drei";
import React,{ Suspense,useState } from "react";
import "./App.css";

/*
3D Model 
title:	(FREE) Porsche 911 Carrera 4S
source:	https://sketchfab.com/3d-models/free-porsche-911-carrera-4s-d01b254483794de3819786d93e0e1ebf
author:	Karol Miklas (https://sketchfab.com/karolmiklas)
*/

const Model = () => {
  const gltf = useLoader(GLTFLoader, "./scene.gltf")
  return (
          <primitive object={gltf.scene} scale={1} />
  );
};

export default function App() {
  const [isHovered, setIsHovered] = useState(false)
  return (
  <div className="App" >
    <VRCanvas >
      <ambientLight intensity={3} />
      <spotLight intensity={5} angle={0.1} penumbra={0.1} position={[10, 15, 10]} />
      <Suspense fallback={null}>
        <Interactive onSelect={() => console.log('clicked!')} onHover={() => setIsHovered(true)} onBlur={() => setIsHovered(false)}>
          <Model />
        </Interactive>
        <OrbitControls />    
      </Suspense>
      <DefaultXRControllers />
    </VRCanvas>
  </div>
  );
}